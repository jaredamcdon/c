#include <stdio.h>

/* integers can be defined in mutliple styles, should be UPPERCASE */

int main(void){
    const int AGE = 21;

    printf("const int AGE: %i\n", AGE);

    #define AGE2 21
    printf("#defined AGE2: %i\n", AGE2);

    /* operators */

    int mod = 0;
    printf("mod: %i\n", mod);
    
    mod+=1;
    printf("mod+=1: %i\n", mod);

    mod++;
    printf("mod++: %i\n", mod);

    mod = mod + 2;
    printf("mod = mod + 2: %i\n", mod);

    mod-=1;
    printf("mod-=1: %i\n", mod);

    mod = mod - 1;
    printf("mod = mod - 1: %i\n", mod);

    mod--;
    printf("mod--: %u\n", mod);

    mod = mod * 8;
    printf("mod = mod * 8: %i\n", mod);
    
    mod *= 8;
    printf("mod *= 8: %i\n", mod);

    mod = mod / 2;
    printf("mod = mod / 2: %i\n", mod);
    
    mod /= 8;
    printf("mod /= 8: %i\n", mod);

    mod = mod % 3;
    printf("mod = mod %% 3: %i\n", mod);

    mod %= 3;
    printf("mod %%= 3: %i\n", mod);
    printf("\n");

    /* logical operators */
    printf("if mod > 0:{ print >0} else{ print <= 0}\n");

    if(mod > 0){
        printf(">0\n");
    } else {
	printf("< 0\n");
    }

    /* switch statements */
    printf("\nswitch for mod 0 -> 3\n");
    switch (mod){
	case 0:
	    printf("mod = 0\n");
	    break;
	case 1:
	    printf("mod = 1\n");
	    break;
	case 2:
	    printf("mod = 2\n");
	    break;
	case 3:
	    printf("mod = 3\n");
	    break;
	default:
	    printf("mod != [0,1,2,3]");
	    break;
    }
    
}
