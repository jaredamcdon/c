#include <stdio.h>

int main(void){
	/* int can hold 2 bytes */
	int int_age = 0;
	printf("int age: %u\n", int_age);
	int_age = 37;
	printf("int (modified) age: %u\n", int_age);
	/* chars can hold numbers between -128 and 127, or one byte */
	char char_age = 127;
	printf("char age: %u\n", char_age);
	/* all number types can overflow, which will make them restart at 0 */
	char_age = 180;
	printf("char (overflow) age: %u\n", char_age);
	/* shorts can hold more than an int but less than a long */
	short short_age = 512;
	printf("short age: %u\n", short_age);
	long long_age = 8888888;
	printf("long age: %ld\n", long_age);

	/* size printing */
	printf("char size: %lu bytes\n", sizeof(char));
	printf("int size: %lu bytes\n", sizeof(int));
	printf("short size: %lu bytes\n", sizeof(short));
	printf("long size: %lu bytes\n", sizeof(long));
	printf("float size: %lu bytes\n", sizeof(float));
	printf("double size: %lu bytes\n", sizeof(double));
	printf("long size: %lu bytes\n", sizeof(long double));
}
