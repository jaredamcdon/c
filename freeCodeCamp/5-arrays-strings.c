#include <stdio.h>
#include <string.h>

int main(void){
    int print_array(int array[], int length){
	printf("\t");
	for (int i = 0; i < length; i++){
            printf("%d", array[i]);
	    if (i == length - 1){
                printf("\n");
	    }
	    else{
	        printf(", ");
	    }
	}
    }
	
    /* define array prices with 5 values */
    int prices[5];

    /* define array alphabet of CONNST sized LETTERS */
    const int LETTERS = 26;
    int alphabet[LETTERS];

    /* initialize array with populated values */

    int ids[3] = {1000, 1001, 1002};
    printf("int ids[3] = {1000, 1001, 1002};\n");
    print_array(ids, 3);
    
    /* assign values to an already defined array prices */
    prices[0] = 1;
    prices[1] = 1;
    prices[2] = 2;
    prices[3] = 5;
    prices[4] = 1;

    printf("int prices[5];\nprices[0] = 1;\nprices[1] = 1;\nprices[2] = 2;\nprices[3] = 5;\nprices[4] = 1;\n");
    print_array(prices, 5);

    /* assign using a loop */
    int ascending_nums[5];

    for (int x = 0; x < 5; x++){
	ascending_nums[x] = x + 1;
    }

    printf("int ascending_nums[5];\nfor (int i = 0; i < 5; i++){\n\tprices[i] = i + 1;\n}\n");
    print_array(ascending_nums,5);
    
    char name[] = "Jared";
    int age = 21;
    char school[] = "IU-PUI";

    printf("%s is %d and goes to %s", name, age, school);
	
    /* these functions are implemented with <string.h> */
    char name_pointer[5];
    strcpy(name_pointer, name);
    printf("\n\nstrcpy(name_pointer, name);\n");
    printf("%s\n", name_pointer);

    strcat(name_pointer, school);
    printf("strcat(name_pointer, school);\n");
    printf("%s\n", name_pointer);
    
    /* bool requires <stdbool.h> */
    int compared = strcmp(name, name_pointer);
    printf("int compared = strcmp(name, name_pointer);\n");
    printf("%d\n", compared);

    int n_compared = strncmp(name, name, 3);
    printf("int n_compared = strncmp(name, name, 3);\n");
    printf("%d\n", n_compared);

    int len = strlen(name);
    printf("int len = strlen(name);\n");
    printf("%d\n", len);

}
