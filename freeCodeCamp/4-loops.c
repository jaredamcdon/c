#include <stdio.h>

int main(void){
    printf("for (int i = 0; i <10, i++){}\n");
    for (int i = 0; i <10; i++){
       	printf("%i, ", i);
    }

    printf("\n\nint x = 0;\nwhile (x<10){ print x; x++}\n");
    int x = 0;
    while (x<10){
        printf("%i, ", x);
	x++;
    }

    printf("\n\nint y = 0;\ndo {print y; y++} while (y <10)\n");
    int y = 0;
    do {
	printf("%i, ", y);
	y++;
    } while (y < 10);

    printf("\n\nfor (int z; z < 10; z++){} WITH break;\n");
    for (int z; z < 10; z++){
        printf("%i", z);
	if(z == 9){
	    break;
	}
	printf(", ");
    }
}
